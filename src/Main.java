import models.*;
import ui.Screen;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        List<PersonalClient> personalClients = generatePersonalClients();
        List<BusinessClient> businessClients = generateBusinessClients(personalClients);

        Screen screen = new Screen(personalClients, businessClients);
        screen.initialize();
    }

    private static List<PersonalClient> generatePersonalClients() {
        List<Address> addresses = Arrays.asList(
                new Address("Main St., Oracabessa", Parish.ST_MARY),
                new Address("Brigade St. Santa Cruz", Parish.ST_ELIZABETH),
                new Address("Brigade St. Santa Cruz", Parish.ST_ELIZABETH),
                new Address("12 High St., Black River", Parish.ST_ELIZABETH),
                new Address("15 Old Hope Road, Kgn 5", Parish.KINGSTON_AND_ST_ANDREW),
                new Address("9 Fearon Ave, May Pen", Parish.CLARENDON),
                new Address("144 Barnett St., Montego Bay", Parish.ST_JAMES),
                new Address("Propsper Rd., Lucea", Parish.HANOVER),
                new Address("Propsper Rd., Lucea", Parish.HANOVER)
        );

        List<Account> accounts = Arrays.asList(
                new Account("2599889", AccountType.SAVINGS, 99900),
                new Account("6909950", AccountType.INVESTMENT, 6484600),
                new Account("1717425", AccountType.DIRECT_BANKING, 6548400),
                new Account("7727498", AccountType.CHEQUING, 487500),
                new Account("4047397", AccountType.DIRECT_BANKING, 85190800),
                new Account("9400350", AccountType.DIRECT_BANKING, 41600),
                new Account("9344342", AccountType.SAVINGS, 4651600),
                new Account("5451119", AccountType.SAVINGS, 654600),
                new Account("5451129", AccountType.SAVINGS, 654600)
        );

        List<Card> cards = Arrays.asList(
                new Card("555555", "5555"),
                new Card("881425", "5007"),
                new Card("333140", "5177"),
                new Card("786049", "8737"),
                new Card("340641", "7200"),
                new Card("317283", "6648"),
                new Card("479916", "7301"),
                new Card("460381", "1712"),
                new Card("444555", "4321")
        );

        List<String[]> personalDataList = Arrays.asList(
                new String[]{"032250868", "JBX09", "Bond", "James", "1-876-987-5583"},
                new String[]{"057921674", "SHW34", "Holmes", "Sherlock", "1-876-759-4441"},
                new String[]{"082701001", "THW03", "Hardy", "Thomas",  "1-876-345-9703"},
                new String[]{"020183246", "NDW58", "Drew", "Nancy", "1-876-859-8089"},
                new String[]{"088443185", "JJX12", "Jones", "Jessica", "1-876-893-5197"},
                new String[]{"038357650", "JBC85", "Bourne", "Jason", "1-876-424-4743"},
                new String[]{"027183279", "HDW05", "Dent", "Harvey", "1-876-484-6108"},
                new String[]{"074788178", "PCW44", "Coulson", "Phil", "1-876-598-2378"},
                new String[]{"074788188", "PCW45", "Admin", "User", "1-876-598-2379"}
        );

        List<PersonalClient> personalClients = new ArrayList<>();
        Date dateOfBirth = new GregorianCalendar(1990, 1, 1).getTime();

        for (int i = 0; i < personalDataList.size(); i++) {
            String[] data = personalDataList.get(i);
            String trn = data[0];
            String clientId = data[1];
            String surname = data[2];
            String givenName = data[3];
            String telephone = data[4];
            Account account = accounts.get(i);
            Address address = addresses.get(i);
            Card card = cards.get(i);
            PersonalClient client;
            if (i != 0) {
                client = new PersonalClient(clientId, trn, address, account, surname, givenName,
                        telephone, dateOfBirth, card);
            } else {
                List<Account> accountList = Arrays.asList(
                        account,
                        new Account("8301980", AccountType.DIRECT_BANKING, 1000),
                        new Account("9301980", AccountType.CHEQUING, 2000),
                        new Account("2301980", AccountType.INVESTMENT, 3500)
                );

                client = new PersonalClient(clientId, trn, address, accountList, surname, givenName,
                        telephone, dateOfBirth, card);
            }
            personalClients.add(client);
        }

        return personalClients;
    }

    private static List<BusinessClient> generateBusinessClients(List<PersonalClient> personalClients) {
        List<Address> addresses = Arrays.asList(
                new Address("14 Constant Sprint Road, Kgn 10", Parish.KINGSTON_AND_ST_ANDREW),
                new Address("15 Ocean Boulevard, Kingston", Parish.KINGSTON_AND_ST_ANDREW),
                new Address("8 Main Street, Savannah-la-mar", Parish.WESTMORELAND),
                new Address("Howard Cook Boulevard, Montego Bay", Parish.ST_JAMES),
                new Address("Chudleigh District, Christiana", Parish.MANCHESTER));

        List<Account> accounts = Arrays.asList(
                new Account("7301980", AccountType.CHEQUING, 142143500),
                new Account("2180949", AccountType.INVESTMENT, 32497500),
                new Account("7326297", AccountType.CHEQUING, 24920900),
                new Account("7525496", AccountType.CHEQUING, 140489300),
                new Account("3488048", AccountType.SAVINGS, 48697500)
        );

        List<String[]> businessDataList = Arrays.asList(
                new String[]{"077733260", "HWA12", "Huwawei Jamaica", "Shenzen Telecoms Caribbean Ltd.", "1-876-633-1859", "1-876-631-5587"},
                new String[]{"085038058", "DIG01", "Digicel", "Mossel Jamaica Ltd.", "1-876-633-1000", "1-876-631-5000"},
                new String[]{"052704967", "WUU88", "Western Photo Ltd.", "Western Photo Ltd.", "1-876-945-6688", "1-876-945-6689"},
                new String[]{"063907629", "SHA47", "Sharkie\'s Seafood Restaurant", "Sharkie\'s Seafood Restaurant", "1-876-954-8469", "1-876-954-8470"},
                new String[]{"060893285", "KPK24", "Sassy Super Salon", "Emily Ramson t/a Sassy Super Salon", "1-876-355-3534", "1-876-355-3535"}
        );

        List<BusinessClient> businessClients = new ArrayList<>();

        for (int i = 0; i < businessDataList.size(); i++) {
            String[] data = businessDataList.get(i);
            String trn = data[0];
            String clientId = data[1];
            String tradingName = data[2];
            String officialName = data[3];
            String telephone = data[4];
            String telephone2 = data[5];
            Account account = accounts.get(i);
            Address address = addresses.get(i);
            PersonalClient signatory = (personalClients == null || personalClients.size() - 1 < i)
                    ? null : personalClients.get(i);
            BusinessClient client = new BusinessClient(clientId, trn, address, account, tradingName, officialName,
                    telephone, telephone2, signatory);
            businessClients.add(client);
        }

//        businessClients.get(0).addAccounts(Arrays.asList(
//                new Account("8301980", AccountType.CHEQUING, 1000),
//                new Account("9301980", AccountType.CHEQUING, 2000),
//                new Account("2301980", AccountType.INVESTMENT, 3500)
//        ));

        return businessClients;
    }
}
