package ui;

import models.*;
import util.SimpleData;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.time.Duration;
import java.util.*;
import java.util.List;
import java.util.concurrent.*;

public class Screen extends JFrame {
    private static final String TITLE = "JLCB Automated Banking Machine";
    private static final String TEXT_BACKSPACE = "Backspace";
    private static final String TEXT_CANCEL = "Cancel";
    private static final String TEXT_CLEAR = "Clear";
    private static final String TEXT_ENTER = "Enter";
    private static final String LEFT_ONE = "left_one";
    private static final String LEFT_TWO = "left_two";
    private static final String LEFT_THREE = "left_three";
    private static final String LEFT_FOUR = "left_four";
    private static final String RIGHT_ONE = "right_one";
    private static final String RIGHT_TWO = "right_two";
    private static final String RIGHT_THREE = "right_three";
    private static final String RIGHT_FOUR = "right_four";

    JFrame mainFrame;

    JButton leftButtonOne = new JButton();
    JButton leftButtonTwo = new JButton();
    JButton leftButtonThree = new JButton();
    JButton leftButtonFour = new JButton();
    JButton rightButtonOne = new JButton();
    JButton rightButtonTwo = new JButton();
    JButton rightButtonThree = new JButton();
    JButton rightButtonFour = new JButton();

    private State currentState = State.ENTER_CARD;

    private JLabel displayLabel;
    private JLabel editTextLabel;

    private List<PersonalClient> personalClients;
    private List<BusinessClient> businessClients;

    private PersonalClient authClient;
    private Account account;
    private long withdrawal;
    private Map<Integer, Integer> deposit = new HashMap<>();
    private Integer depositKey;
    private List<Account> availableAccounts = new ArrayList<>();

    public Screen(List<PersonalClient> personalClients, List<BusinessClient> businessClients) throws HeadlessException {
        this.personalClients = personalClients;
        this.businessClients = businessClients;
        initializeDeposit();
    }

    private void initializeDeposit() {
        deposit.clear();
        deposit.put(100, 0);
        deposit.put(500, 0);
        deposit.put(1000, 0);
        deposit.put(5000, 0);
    }

    private AbstractAction touchScreenActionListener = new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (currentState == State.SHOW_LOGIN_ERROR && event.getActionCommand().equals(RIGHT_FOUR)) {
                goToScreen(State.ENTER_CARD);
            }

            if (currentState == State.ENTER_CARD && event.getActionCommand().equals(RIGHT_FOUR)) {
                String cardNumber = editTextLabel.getText();
                System.out.println(cardNumber);

                for (PersonalClient client: personalClients){
                    Card card = client.getCard();
                    if (card.getSerialNumber().equals(cardNumber)) {
                        authClient = client;
                        goToScreen(State.ENTER_PIN);
                        return;
                    }
                }

                if (authClient == null) {
                    System.out.println("Wrong card number");
                    goToScreen(State.SHOW_LOGIN_ERROR);
                }
            }

            if (currentState == State.ENTER_PIN && event.getActionCommand().equals(RIGHT_FOUR)) {
                String pin = editTextLabel.getText();
                editTextLabel.setText("");

                Card card = authClient.getCard();

                if ( ! card.getPin().equals(pin)) {
                    goToScreen(State.SHOW_LOGIN_ERROR);
                    return;
                }

                // exit for admin account
                if (card.getSerialNumber().equals("444555") && card.getPin().equals("4321")) {
                    mainFrame.dispatchEvent(new WindowEvent(mainFrame, WindowEvent.WINDOW_CLOSING));
                    return;
                }

                goToScreen(State.SELECT_PERSONAL_ACCOUNTS);
                return;
            }

            if (currentState == State.SHOW_MAIN_MENU) {
                if (event.getActionCommand().equals(RIGHT_ONE)) {
                    goToScreen(State.SHOW_BALANCE);
                } else if (event.getActionCommand().equals(RIGHT_TWO) && account.getType() != AccountType.INVESTMENT) {
                    goToScreen(State.ENTER_WITHDRAW_AMOUNT);
                } else if (event.getActionCommand().equals(RIGHT_THREE)) {
                    goToScreen(State.ENTER_DEPOSIT_SUMMARY);
                }
                return;
            }

            if (currentState == State.SHOW_BALANCE && event.getActionCommand().equals(RIGHT_FOUR)) {
                goToScreen(State.SHOW_EXIT_SCREEN);
                return;
            }

            if (currentState == State.ENTER_WITHDRAW_AMOUNT && event.getActionCommand().equals(RIGHT_FOUR)) {
                withdrawal = Integer.valueOf(editTextLabel.getText());
                if (withdrawal % 100 != 0) {
                    displayLabel.setText("Enter withdrawal amount. (Must be a multiple of 100)\n" +
                            "Error, ensure value is multiple of 100");
                    withdrawal = 0;
                    return;
                }

                if (account.currentBalance() - 100 < withdrawal) {
                    editTextLabel.setText("Enter withdrawal amount. (Must be a multiple of 100)\n" +
                            "Error, remaining balance will be less than $100");
                    withdrawal = 0;
                    return;
                }

                goToScreen(State.SHOW_WITHDRAW_CONFIRMATION);
                return;
            }

            if (currentState == State.SHOW_WITHDRAW_CONFIRMATION) {
                if (event.getActionCommand().equals(RIGHT_THREE)) {
                    withdrawal = 0;
                }
                if (event.getActionCommand().equals(RIGHT_FOUR)) {
                    withdrawal = Long.valueOf(editTextLabel.getText());
                    saveWithdrawal(account, withdrawal);
                }
                goToScreen(State.SHOW_EXIT_SCREEN);
                return;
            }

            if (currentState == State.ENTER_DEPOSIT_SUMMARY) {
                if (event.getActionCommand().equals(RIGHT_THREE)) {
                    goToScreen(State.SHOW_EXIT_SCREEN);
                    return;
                }

                if (event.getActionCommand().equals(LEFT_ONE)) {
                    depositKey = 5000;
                } else if (event.getActionCommand().equals(LEFT_TWO)) {
                    depositKey = 1000;
                } else if (event.getActionCommand().equals(LEFT_THREE)) {
                    depositKey = 500;
                } else if (event.getActionCommand().equals(LEFT_FOUR)) {
                    depositKey = 100;
                } else if (event.getActionCommand().equals(RIGHT_FOUR)) {
                    saveDeposit(account, deposit);
                    goToScreen(State.SHOW_EXIT_SCREEN);
                    return;
                }

                if (depositKey != null) {
                    goToScreen(State.ENTER_DEPOSIT_AMOUNT);
                    return;
                }
                return;
            }

            if (currentState == State.ENTER_DEPOSIT_AMOUNT) {
                if (event.getActionCommand().equals(LEFT_FOUR)) {
                    goToScreen(State.ENTER_DEPOSIT_SUMMARY);
                }

                if (event.getActionCommand().equals(RIGHT_FOUR)) {
                    int value = Integer.valueOf(editTextLabel.getText());
                    deposit.put(depositKey, value);
                    goToScreen(State.ENTER_DEPOSIT_SUMMARY);
                }
                return;
            }

            if (currentState == State.SHOW_EXIT_SCREEN) {
                if (event.getActionCommand().equals(RIGHT_FOUR)) {
                    goToScreen(State.SELECT_PERSONAL_ACCOUNTS);
                } else if (event.getActionCommand().equals(RIGHT_THREE)) {
                    goToScreen(State.ENTER_CARD);
                }
                return;
            }

            if (currentState == State.SELECT_PERSONAL_ACCOUNTS) {
                if (event.getActionCommand().equals(RIGHT_THREE) && hasBusinessAccounts(authClient)) {
                    goToScreen(State.SELECT_BUSINESS_ACCOUNTS);
                } else if (event.getActionCommand().equals(RIGHT_FOUR)) {
                    goToScreen(State.ENTER_CARD);
                } else {
                    List<String> strings = Arrays.asList(LEFT_ONE, LEFT_TWO, LEFT_THREE, LEFT_FOUR, RIGHT_ONE, RIGHT_TWO);
                    int personalAccountIndex = strings.indexOf(event.getActionCommand());

                    if (personalAccountIndex != -1) {
                        account = authClient.getAccounts().get(personalAccountIndex);
                        goToScreen(State.SHOW_MAIN_MENU);
                        return;
                    }
                }
                return;
            }

            if (State.SELECT_BUSINESS_ACCOUNTS == currentState) {
                if (event.getActionCommand().equals(RIGHT_THREE) && hasBusinessAccounts(authClient)) {
                    goToScreen(State.SELECT_PERSONAL_ACCOUNTS);
                } else if (event.getActionCommand().equals(RIGHT_FOUR)) {
                    goToScreen(State.ENTER_CARD);
                } else {
                    List<String> strings = Arrays.asList(LEFT_ONE, LEFT_TWO, LEFT_THREE, LEFT_FOUR, RIGHT_ONE, RIGHT_TWO);
                    int businessAccountIndex = strings.indexOf(event.getActionCommand());

                    if (businessAccountIndex != -1) {
                        account = authClient.getAccounts().get(businessAccountIndex);
                        goToScreen(State.SHOW_MAIN_MENU);
                        return;
                    }
                }
                return;
            }
        }
    };

    private void saveDeposit(Account account, Map<Integer, Integer> deposit) {
        long total = 0;
        List<Integer> billTypes = new ArrayList<>(deposit.keySet());

        for (Integer billType : billTypes) {
            total += (billType * deposit.get(billType));
        }

        Transaction transaction = new Transaction(account.getAccountId(), TransactionType.DEPOSIT,
                authClient.getClientId(), total);

        try {
            SimpleData.appendCSV("src/data/transactions.csv", transaction.toArray());
            account.deposit(total);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveWithdrawal(Account account, long withdrawal) {
        Transaction transaction = new Transaction(account.getAccountId(), TransactionType.WITHDRAWAL,
                authClient.getClientId(), withdrawal);

        try {
            SimpleData.appendCSV("src/data/transactions.csv", transaction.toArray());
            account.withdraw(withdrawal);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private AbstractAction numberPadActionListener = new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent event) {
            String text = editTextLabel.getText() + event.getActionCommand();
            editTextLabel.setText(text);
        }
    };

    private AbstractAction operationActionListener = new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent event) {
            String text = displayLabel.getText();

            switch (event.getActionCommand()) {
                case TEXT_BACKSPACE:
                    if ( ! text.isEmpty())
                    editTextLabel.setText(text.substring(0, text.length() - 1));
                    break;
                case TEXT_CLEAR:
                    editTextLabel.setText("");
                    break;
                case TEXT_CANCEL:

                    break;
                case TEXT_ENTER:

                    break;
            }
        }
    };

    public void initialize() {
        JPanel touchPanel = initializeTouchDisplay();
        JPanel numberPadPanel = initializeNumberPad();
        JPanel optionsPanel = initializeOptionsPanel();

        JPanel bottomPanel = new JPanel();
        bottomPanel.setLayout(new GridLayout(1, 2,150,10));
        bottomPanel.add(numberPadPanel);
        bottomPanel.add(optionsPanel);

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout (new GridLayout (2, 0, 5, 10));
        mainPanel.setBorder (BorderFactory.createEmptyBorder (8, 8, 8, 8));

        mainPanel.add(touchPanel);
        mainPanel.add(bottomPanel);

        mainFrame = new JFrame (TITLE);
        mainFrame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
        mainFrame.getContentPane().add (mainPanel);
        mainFrame.pack();
        mainFrame.setVisible(true);

        goToScreen(State.ENTER_CARD);
    }

    private void goToScreen(State newState) {
        switch (newState) {
            case ENTER_CARD:
                showCardNumberEntryScreen();
                break;
            case ENTER_PIN:
                showPinNumberEntryScreen();
                break;
            case SHOW_LOGIN_ERROR:
                showLoginErrorScreen();
                break;
            case SELECT_PERSONAL_ACCOUNTS:
                showPersonalAccountsOptionScreen();
                break;
            case SHOW_MAIN_MENU:
                showMainMenuScreen();
                break;
            case SHOW_BALANCE:
                showBalanceScreen();
                break;
            case ENTER_WITHDRAW_AMOUNT:
                showWithdrawScreen();
                break;
            case ENTER_DEPOSIT_SUMMARY:
                showDepositSummaryScreen();
                break;
            case ENTER_DEPOSIT_AMOUNT:
                showDepositAmountScreen();
                break;
            case SHOW_WITHDRAW_CONFIRMATION:
                showWithdrawalConfirmationScreen();
                break;
            case SHOW_EXIT_SCREEN:
                showExitScreen();
                break;
            case SELECT_BUSINESS_ACCOUNTS:
                showBusinessAccountsOptionScreen();
        }

        currentState = newState;
        System.out.println(currentState);
    }

    private boolean hasBusinessAccounts(PersonalClient personalClient) {
        for (BusinessClient client : businessClients) {
            if (client.getSignatories().contains(personalClient)) {
                return true;
            }
        }
        return false;
    }

    private void showBusinessAccountsOptionScreen() {
        clearTouchDisplayButtons();

        List<Account> businessAccounts = new ArrayList<>();

        for (BusinessClient client : businessClients) {
            if (client.getSignatories().contains(authClient)) {
                businessAccounts.addAll(client.getAccounts());
            }
        }

        if (businessAccounts.size() > 6) {
            businessAccounts = businessAccounts.subList(0, 6);
        }

        List<JButton> buttons = Arrays.asList(leftButtonOne, leftButtonTwo, leftButtonThree, leftButtonFour,
                rightButtonOne, rightButtonTwo);
        List<String> labels = Arrays.asList("A", "B", "C", "D", "E", "F");

        String displayText = "Select A Business Account";
        displayText = updateAccountSelectionButtons(businessAccounts, buttons, labels, displayText);

        rightButtonThree.setText("Personal Accounts");

        rightButtonFour.setText("Cancel");
        displayLabel.setText(displayText);
        editTextLabel.setText("");
    }

    private String updateAccountSelectionButtons(List<Account> businessAccounts, List<JButton> buttons, List<String> labels, String displayText) {
        for (int i = 0; i < businessAccounts.size(); i++) {
            Account availableAccount = businessAccounts.get(i);
            String label = labels.get(i);
            buttons.get(i).setText(label);
            displayText += "\n" + label + ". " + availableAccount.getAccountId() + " " + availableAccount.getType();
        }
        return displayText;
    }

    private void showPersonalAccountsOptionScreen() {
        clearTouchDisplayButtons();

        boolean hasBusinessAccounts = hasBusinessAccounts(authClient);

        availableAccounts = authClient.getAccounts();

        int numberOfAccounts = availableAccounts.size();

        if (numberOfAccounts < 1) {
            goToScreen(State.SHOW_LOGIN_ERROR);
            return;
        }

        if (numberOfAccounts == 1 && ! hasBusinessAccounts) {
            account = authClient.getAccounts().get(0);
            goToScreen(State.SHOW_MAIN_MENU);
            return;
        }

        List<JButton> buttons = Arrays.asList(leftButtonOne, leftButtonTwo, leftButtonThree, leftButtonFour,
                rightButtonOne, rightButtonTwo);
        List<String> labels = Arrays.asList("A", "B", "C", "D", "E", "F");

        String displayText = "Select A Personal Account";

        displayText = updateAccountSelectionButtons(availableAccounts, buttons, labels, displayText);

        if (hasBusinessAccounts) {
            rightButtonThree.setText("Business Accounts");
        }

        rightButtonFour.setText("Cancel");
        displayLabel.setText(displayText);
        editTextLabel.setText("");
    }

    private void showExitScreen() {
        clearTouchDisplayButtons();
        initializeDeposit();
        rightButtonThree.setText("No");
        rightButtonFour.setText("Yes");
        displayLabel.setText("Do you have any more transactions?");
    }

    private void showDepositAmountScreen() {
        clearTouchDisplayButtons();
        displayLabel.setText("Enter how many $" + depositKey + " bills to deposit");
        editTextLabel.setText("");
        rightButtonFour.setText("Continue");
        leftButtonFour.setText("Back");
    }

    private void showWithdrawalConfirmationScreen() {
        String text = "Withdrawal total: $" + withdrawal;

        long multiple5000 = withdrawal / 5000;
        withdrawal -= (multiple5000 * 5000);

        if (multiple5000 > 0) {
            text += "\n$5000 x " + multiple5000;
        }

        long multiple1000 = withdrawal / 1000;
        withdrawal -= (multiple1000 * 1000);

        if (multiple1000 > 0) {
            text += "\n$1000 x " + multiple1000;
        }

        long multiple500 = withdrawal / 500;
        withdrawal -= (multiple500 * 500);

        if (multiple500 > 0) {
            text += "\n$500 x " + multiple500;
        }

        long multiple100 = withdrawal / 100;

        if (multiple100 > 0) {
            text += "\n$100 x " + multiple100;
        }

        displayLabel.setText(text);
        rightButtonThree.setText("Cancel");
        rightButtonFour.setText("Continue");
    }

    private void showDepositSummaryScreen() {
        clearTouchDisplayButtons();
        leftButtonOne.setText("A");
        leftButtonTwo.setText("B");
        leftButtonThree.setText("C");
        leftButtonFour.setText("D");
        String text = "Enter select a letter to enter the type of bill\n";
        text += "\nA - $5000 x " + deposit.get(5000);
        text += "\nB - $1000 x " + deposit.get(1000);
        text += "\nC - $500 x " + deposit.get(500);
        text += "\nD - $100 x " + deposit.get(100);
        displayLabel.setText(text);
        rightButtonThree.setText("Cancel");
        rightButtonFour.setText("Confirm");
    }

    private void showWithdrawScreen() {
        clearTouchDisplayButtons();
        rightButtonFour.setText("Continue");
        displayLabel.setText("Enter withdrawal amount. (Must be a multiple of 100)");
        editTextLabel.setText("");
    }

    private void showBalanceScreen() {
        clearTouchDisplayButtons();
        String displayText = String.format("The account balance for %s is:\n \t$%.2f",
                account.getAccountId(),
                account.currentBalance());
        displayLabel.setText(displayText);
        rightButtonFour.setText("Continue");
    }

    private void showMainMenuScreen() {
        clearTouchDisplayButtons();
        displayLabel.setText("Main Menu");
        editTextLabel.setText("");
        rightButtonOne.setText("Balance Query");
        rightButtonTwo.setText(account.getType() == AccountType.INVESTMENT ? "" : "Withdraw");
        rightButtonThree.setText("Deposit");
    }

    private void showLoginErrorScreen() {
        clearTouchDisplayButtons();
        rightButtonFour.setText("Continue");
        authClient = null;
        account = null;
        initializeDeposit();
        withdrawal = 0;
        displayLabel.setText("Unknown Card or Invalid PIN");

        // TODO needs a delay

        goToScreen(State.ENTER_CARD);
    }

    private void showPinNumberEntryScreen() {
        clearTouchDisplayButtons();
        displayLabel.setText("Enter PIN Number");
        editTextLabel.setText("");
        rightButtonFour.setText("Continue");
    }

    private void showCardNumberEntryScreen() {
        authClient = null;
        account = null;
        clearTouchDisplayButtons();
        displayLabel.setText("Enter Card Number");
        editTextLabel.setText("");
        rightButtonFour.setText("Continue");
    }

    private void clearTouchDisplayButtons() {
        java.util.List<JButton> touchDisplayButtons = Arrays.asList(
                leftButtonOne, leftButtonTwo, leftButtonThree, leftButtonFour,
                rightButtonOne, rightButtonTwo, rightButtonThree, rightButtonFour
        );

        for (JButton button : touchDisplayButtons) {
            button.setText("");
        }
    }

    private JPanel initializeOptionsPanel() {
        JButton backspaceButton = new JButton(TEXT_BACKSPACE);
        JButton cancelButton = new JButton(TEXT_CANCEL);
        JButton clearButton = new JButton(TEXT_CLEAR);
        JButton enterButton = new JButton(TEXT_ENTER);

        java.util.List<JButton> optionButtons = Arrays.asList(backspaceButton, clearButton);

        for (JButton button : optionButtons) {
            button.addActionListener(operationActionListener);;
            button.setPreferredSize(new Dimension(40,40));
        }

        JPanel optionsPanel = new JPanel();
        optionsPanel.setLayout(new GridLayout(4, 1,50,50));
        optionsPanel.add(backspaceButton);
        optionsPanel.add(clearButton);
        optionsPanel.add(cancelButton);
        optionsPanel.add(enterButton);

        return optionsPanel;
    }

    private JPanel initializeNumberPad() {
        JButton zeroButton = new JButton("0");
        JButton oneButton = new JButton("1");
        JButton twoButton = new JButton("2");
        JButton threeButton = new JButton("3");
        JButton fourButton = new JButton("4");
        JButton fiveButton = new JButton("5");
        JButton sixButton = new JButton("6");
        JButton sevenButton = new JButton("7");
        JButton eightButton = new JButton("8");
        JButton nineButton = new JButton("9");

        JButton spaceButton = new JButton(" ");
        JButton dotButton = new JButton(".");

        java.util.List<JButton> numericButtons = Arrays.asList(zeroButton, oneButton,
                twoButton, threeButton, fourButton, fiveButton, sixButton, sevenButton,
                eightButton, spaceButton, nineButton, dotButton);

        for (JButton button : numericButtons) {
            button.addActionListener(numberPadActionListener);
        }

        JPanel numberPadPanel = new JPanel();
        numberPadPanel.setLayout(new GridLayout(4, 3,10, 20));

        numberPadPanel.add(oneButton);
        numberPadPanel.add(twoButton);
        numberPadPanel.add(threeButton);

        numberPadPanel.add(fourButton);
        numberPadPanel.add(fiveButton);
        numberPadPanel.add(sixButton);

        numberPadPanel.add(sevenButton);
        numberPadPanel.add(eightButton);
        numberPadPanel.add(nineButton);

        numberPadPanel.add(spaceButton);
        numberPadPanel.add(zeroButton);
        numberPadPanel.add(dotButton);

        return numberPadPanel;
    }

    private JPanel initializeTouchDisplay() {
        leftButtonOne.setActionCommand(LEFT_ONE);
        leftButtonTwo.setActionCommand(LEFT_TWO);
        leftButtonThree.setActionCommand(LEFT_THREE);
        leftButtonFour.setActionCommand(LEFT_FOUR);
        rightButtonOne.setActionCommand(RIGHT_ONE);
        rightButtonTwo.setActionCommand(RIGHT_TWO);
        rightButtonThree.setActionCommand(RIGHT_THREE);
        rightButtonFour.setActionCommand(RIGHT_FOUR);

        java.util.List<JButton> touchDisplayButtons = Arrays.asList(
                leftButtonOne, leftButtonTwo, leftButtonThree, leftButtonFour,
                rightButtonOne, rightButtonTwo, rightButtonThree, rightButtonFour
        );

        for (JButton touchDisplayButton : touchDisplayButtons) {
            touchDisplayButton.addActionListener(touchScreenActionListener);
        }

        JPanel touchPanel = new JPanel();
        touchPanel.setLayout(new GridLayout(0, 3));

        JPanel leftTouchPanel = new JPanel();
        JPanel centerTouchPanel = new JPanel();
        JPanel rightTouchPanel = new JPanel();

        leftTouchPanel.setLayout(new GridLayout(4, 1,0,30));
        rightTouchPanel.setLayout(new GridLayout(4, 1,0,30));

        leftTouchPanel.add(leftButtonOne);
        leftTouchPanel.add(leftButtonTwo);
        leftTouchPanel.add(leftButtonThree);
        leftTouchPanel.add(leftButtonFour);

        rightTouchPanel.add(rightButtonOne);
        rightTouchPanel.add(rightButtonTwo);
        rightTouchPanel.add(rightButtonThree);
        rightTouchPanel.add(rightButtonFour);

        JPanel displayPanel = new JPanel();
        displayPanel.setSize(new Dimension(960, 480));
        displayPanel.setLayout(new GridLayout(2, 1,30,0));
        displayPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        displayLabel = new JLabel();
        editTextLabel = new JLabel();

        displayPanel.add(displayLabel);
        displayPanel.add(editTextLabel);

        centerTouchPanel.add(displayPanel);

        touchPanel.add(leftTouchPanel);
        touchPanel.add(centerTouchPanel);
        touchPanel.add(rightTouchPanel);

        return touchPanel;
    }
}
