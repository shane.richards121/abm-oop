package models;

public class Card {
    private String serialNumber;
    private String pin;

    public Card(String serialNumber, String pin) {
        this.serialNumber = serialNumber;
        this.pin = pin;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getPin() {
        return pin;
    }
}
