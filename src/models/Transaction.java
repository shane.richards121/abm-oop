package models;

public class Transaction {
    private String accountId;
    private TransactionType type;
    private String clientId;
    private long amount;

    public Transaction(String accountId, TransactionType type, String clientId, long amount) {
        this.accountId = accountId;
        this.type = type;
        this.clientId = clientId;
        this.amount = amount;
    }

    public String[] toArray() {
        return new String[]{ accountId, type.toString(), String.valueOf(amount), clientId };
    }
}
