package models;

import java.util.Collections;
import java.util.List;

public class BusinessClient extends Client {
    private String tradingName;
    private String officialName;
    private String telephone;
    private String telephone2;
    private List<PersonalClient> signatories;

    public BusinessClient(String clientId, String trn, Address address, Account account,
                          String tradingName, String officialName, String telephone, String telephone2,
                          List<PersonalClient> signatories) {
        super(clientId, trn, address, account);
        this.tradingName = tradingName;
        this.officialName = officialName;
        this.telephone = telephone;
        this.telephone2 = telephone2;
        this.signatories = signatories;
    }

    public BusinessClient(String clientId, String trn, Address address, Account account,
                          String tradingName, String officialName, String telephone, String telephone2,
                          PersonalClient signatory) {
        this(clientId, trn, address, account, tradingName, officialName, telephone, telephone2, Collections.singletonList(signatory));
    }

    @Override
    public String getName() {
        return tradingName;
    }

    @Override
    public String getFullName() {
        return officialName;
    }

    public String getTradingName() {
        return tradingName;
    }

    public String getOfficialName() {
        return officialName;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getTelephone2() {
        return telephone2;
    }

    public List<PersonalClient> getSignatories() {
        return signatories;
    }

}
