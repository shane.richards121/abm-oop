package models;

import java.util.Date;
import java.util.List;

public class PersonalClient extends Client {
    private String surname;
    private String givenName;
    private String telephone;
    private Date dob;
    private Card card;

    public PersonalClient(String clientId, String trn, Address address, Account account,
                          String surname, String givenName, String telephone, Date dob, Card card) {
        super(clientId, trn, address, account);
        this.surname = surname;
        this.givenName = givenName;
        this.telephone = telephone;
        this.dob = dob;
        this.card = card;
    }

    public PersonalClient(String clientId, String trn, Address address, List<Account> accounts,
                          String surname, String givenName, String telephone, Date dob, Card card) {
        super(clientId, trn, address, accounts);
        this.surname = surname;
        this.givenName = givenName;
        this.telephone = telephone;
        this.dob = dob;
        this.card = card;
    }


    @Override
    public String getName() {
        return String.format("%s %s", givenName, surname);
    }

    @Override
    public String getFullName() {
        return null;
    }

    @Override
    public String getTelephone() {
        return telephone;
    }

    public Date getDob() {
        return dob;
    }

    public Card getCard() {
        return card;
    }
}
