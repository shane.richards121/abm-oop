package models;

public enum Parish {
    KINGSTON_AND_ST_ANDREW("Kingston and St. Andrew"),
    ST_THOMAS ("St. Thomas"),
    PORTLAND ("Portland"),
    ST_MARY("St. Mary"),
    ST_ANN ("St. Ann"),
    TRELAWNY("Trelawny"),
    ST_JAMES ("St. James"),
    HANOVER("Hanover"),
    WESTMORELAND("Westmoreland"),
    ST_ELIZABETH ("St. Elizabeth"),
    MANCHESTER("Manchester"),
    CLARENDON("Clarendon"),
    ST_CATHERINE("St. Catherine");

    private final String name;

    Parish(String name) {
        this.name = name;
    }

    public String fullName() {
        return this.name;
    }

    @Override
    public String toString() {
        return name;
    }

    public static Parish findByName(String name){
        for (Parish pq: Parish.values()){
            if (name.equalsIgnoreCase(pq.fullName())){
                return pq;
            }
        }
        return Parish.KINGSTON_AND_ST_ANDREW;
    }
}
