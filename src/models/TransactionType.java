package models;

public enum TransactionType {
    WITHDRAWAL( "withdrawal"),
    DEPOSIT ("deposit");

    private String type;

    TransactionType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
}
