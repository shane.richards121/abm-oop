package models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class Client {
    private String clientId;
    private List<Address> addresses = new ArrayList<>();
    private List<Account> accounts = new ArrayList<>();
    private String trn;

    public Client(String clientId, String trn, List<Address> addresses, List<Account> accounts) {
        this.clientId = clientId;
        this.addresses = addresses;
        this.accounts = accounts;
        this.trn = trn;
    }

    public Client(String clientId, String trn, Address address, Account account) {
        this(clientId, trn,
                (address == null) ? new ArrayList<>() : Collections.singletonList(address),
                (account == null) ? new ArrayList<>() : Collections.singletonList(account));
    }

    public Client(String clientId, String trn, Address address, List<Account> accounts) {
        this(clientId, trn,
                (address == null) ? new ArrayList<>() : Collections.singletonList(address),
                accounts);
    }

    public Address getPrimaryAddress() {
        if (addresses.size() > 0) {
            return addresses.get(0);
        } else {
            return null;
        }
    }

    public String getClientId() {
        return clientId;
    }

    public abstract String getName();

    public abstract String getFullName();

    public abstract String getTelephone();

    public String getTRN() {
        return trn;
    }

    public void addAccount(Account account) {
        accounts.add(account);
    }

    public void addAccount(String accountId, AccountType type, double balance) {
        Account account = new Account(accountId, type, balance);
        this.addAccount(account);
    }

    public void addAccounts(List<Account> accounts) {
        this.accounts.addAll(accounts);
    }

    public void addAddress(Address address) {
        addresses.add(address);
    }

    public Account getAccount(int index) {
        if (index > accounts.size() - 1) {
            return null;
        }
        return accounts.get(index);
    }

    public List<Account> getAccounts(){
        return accounts;
    }

    public Parish getPrimaryParish() {
        if (addresses.isEmpty()) {
            return null;
        }
        Address  address = addresses.get(0);
        return address.getParish();
    }


    public String toString(){
        String f = "Client: %s (%s)\n  Tel: %s%s%s";
        StringBuilder aa, ac;
        ac = new StringBuilder("\n  Accounts:");
        for (Account a : accounts){
            ac.append(String.format("\n    %-20s:    $%,12.2f", a.getType(), a.currentBalance()));
        }
        aa = new StringBuilder("\n  Address:");
        for (Address b: addresses){
            aa.append(String.format("\n    %s \n", b.toString()));
        }
        return String.format(f, getName(), clientId, getTelephone(), ac.toString(), aa.toString());

    }
}
