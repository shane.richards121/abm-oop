package models;

public class Account {
    private String accountId;
    private AccountType type;
    private double balance;

    public Account(String accountId, AccountType type, double balance) {
        this.accountId = accountId;
        this.type = type;
        this.balance = balance;
    }

    public Account(String accountId, AccountType type) {
        this(accountId, type, 0);
    }

    public String getAccountId() {
        return accountId;
    }

    public void withdraw(double amount) {
        this.balance -= amount;
    }

    public void deposit(double amount) {
        this.balance += amount;
    }

    public AccountType getType() {
        return type;
    }

    public double currentBalance() {
        return balance;
    }

    public double calcInterest(int days) {
        double DAYS_IN_YEAR = 365.25, interest;
        interest = balance * type.interestRate();
        return interest * ((double) days / DAYS_IN_YEAR);
    }

    public double calculateTax() {
        return 0; // TODO
    }
}
