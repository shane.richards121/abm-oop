package models;

public class Address {
    private String address;
    private Parish parish;

    public Address(String address, Parish parish) {
        this.address = address;
        this.parish = parish;
    }

    public Address(String street, String parishName) {
        this(street, Parish.findByName(parishName));
    }

    public String getAddress() {
        return String.format("%s, %s", address, parish);
    }

    public Parish getParish() {
        return parish;
    }

}
