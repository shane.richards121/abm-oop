package models;

public enum AccountType {
    SAVINGS ("Savings", 0.05),
    DIRECT_BANKING ("Direct Banking", 0),
    INVESTMENT ("Investment", 0.15),
    CHEQUING ("Chequing", 0);

    private final String name;
    private final double interestRate;

    AccountType(String name, double interestRate) {
        this.name = name;
        this.interestRate = interestRate;
    }

    public double interestRate() {
        return interestRate;
    }

    public String getName() {
        return name;
    }
}
